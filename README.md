# My Synology CLI Reminder

## Mise à jour de DSM
```
sudo synoupgrade --check
sudo synoupgrade --download
sudo synoupgrade --start
```

## Opérations sur les paquets

### Installation, désinstallation, mise à jour etc.
```
sudo synopkg install <spk>
sudo synopkg uninstall <package>
sudo synopkg checkupdate <package>
sudo synopkg checkupdateall
sudo synopkg upgradeall
```

### Démarrage, arrêt, statut, etc.
```
sudo synopkg start <package>
sudo synopkg stop <package>
sudo synopkg restart <package>
sudo synopkg resume <package>
sudo synopkg pause <package>
sudo synopkg status <package>
sudo synopkg is_onoff <package>
```

### Maintenance et debugging
```
sudo synopkg version <package>
sudo synopkg query <spk>
sudo synopkg list
sudo synopkg log <package>
```

## Gestion des services
```
synoservicecfg --list
synoservice --status
synoservicecfg --stop <service>
synoservicecfg --hard-stop <service>
synoservicecfg --start <service>
synoservicecfg --hard-start <service>
synoservice --restart <service>
synoservicectl --restart <service>
```

## Cron
Editer le fichier `/etc/crontab`, puis :
```
synoservice -restart crond
```

## Sources et compléments

### Ligne de commande
- https://www.synology.com/en-global/knowledgebase/DSM/help/DSM/AdminCenter/system_terminal
- https://global.download.synology.com/download/Document/Software/DeveloperGuide/Firmware/DSM/All/enu/Synology_DiskStation_Administration_CLI_Guide.pdf
- https://global.download.synology.com/download/Document/Software/DeveloperGuide/Firmware/DSM/All/enu/Synology_DiskStation_Administration_CLI_Guide.pdf
- https://www.going-flying.com/blog/upgrading-synology-dsm-from-the-command-line.html
- https://tech.setepontos.com/2018/03/25/control-synology-dsm-services-via-terminal-ssh/
- https://community.synology.com/enu/forum/17/post/117671
- https://community.synology.com/enu/forum/17/post/96628
- https://community.synology.com/enu/forum/17/post/117671
- https://community.synology.com/enu/forum/17/post/77374
- https://github.com/SynoCommunity/spksrc/issues/3403
- https://www.cachem.fr/synology-installer-ipkg/
- https://tech.setepontos.com/2018/03/25/control-synology-dsm-services-via-terminal-ssh/

### Pare-feu et sécurité
- https://miketabor.com/securing-synology-nas/
- https://www.numetopia.fr/comment-activer-et-configurer-le-firewall-du-synology/
- https://news.infomaniak.com/10-methodes-pour-securiser-lacces-de-votre-nas-synology/
- https://www.geeek.org/securite-astuces-securite-nas-synology-179/

### Cron

- http://www.multigesture.net/articles/how-to-use-cron-on-a-synology-nas/
- http://sam.web.free.fr/blog/?p=2099
